import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { passwordValidator } from '../shared/password.validator';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit{

  registrationForm!:FormGroup

  
  constructor(private fb:FormBuilder){}

  onSubmit(){
    console.log(this.registrationForm.value)
  }

  ngOnInit(): void {
    this.registrationForm = this.fb.group({
      name:['', Validators.required],
      email:['', Validators.required],
      password:['', Validators.required],
      confirm_password:['', Validators.required]
    }, {validators:passwordValidator})
  
  }

  get formField(){
    return this.registrationForm.controls
  }

}
