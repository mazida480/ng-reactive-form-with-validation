import { AbstractControl } from "@angular/forms";

export function passwordValidator(control:AbstractControl):{[key:string]:boolean} | null {

    const password = control.get('password')
    const confirmPassword = control.get('confirm_password')

    if(password?.dirty && confirmPassword?.pristine){
        return null;
    }

    return password && confirmPassword && password.value !== confirmPassword.value ? {'misMatch':true} : null ;

}